

	<?php

		// check if the repeater field has rows of data
		if( have_rows('property') ):

			// loop through the rows of data
			while ( have_rows('property') ) : the_row(); ?>
			<div class="col-md-4 col-sm-4 col-xs-12">

				<div class="box">

				<a href="<?php the_sub_field('property_url'); ?>" title="<?php the_sub_field('propery_name'); ?>">
					<img src="<?php the_sub_field('property_image'); ?>" class="img-responsive">
					<div class="box_content">
						<img src="<?php the_sub_field('property_logo'); ?>" class="img-logo">
						<h4><?php the_sub_field('propery_name'); ?></h4>
					</div>
				</a>

				</div>
			</div>
		<?php
			endwhile;

		else :

			// no rows found

		endif;

	?>
