<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="asset/img/favicon.ico">
	<title>Hestia Connecting Hotel</title>

	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=EB+Garamond:400,500" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	    crossorigin="anonymous">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
	    crossorigin="anonymous">
	<link href="<?php bloginfo('template_url'); ?>/asset/fontello/css/marker.css" rel="stylesheet" type="text/css" media="all">
	<link href="<?php echo get_stylesheet_uri(); ?>" rel="stylesheet" type="text/css" media="all">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />

	<?php wp_head(); ?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body>

	<div id="wrapper" class="homepage">

		<div class="section main-slider">
			<?php

			$images = get_field('slider');

			if( $images ): ?>
			    <div id="slider" class="flexslider">
			        <ul class="slides">
			            <?php foreach( $images as $image ): ?>
			                <li style="background-image: url('<?php echo $image['url']; ?>')">
			                </li>
			            <?php endforeach; ?>
			        </ul>
			    </div>
			<?php endif; ?>

			<div class="logo-box">
				<?php if( get_field('image') ): ?>
					<img src="<?php the_field('image'); ?>" />
				<?php endif; ?>
			</div>

		</div>

		<div class="section section__block content-intro">
			<div class="container">
				<div class="sectitle">
					<h2>About Us</h2>
				</div>
				<p><?php the_field('content-main'); ?></p>
				<div class="desc-company">
          <div class="row">
            <div class="col-md-2 col-sm-6">
              <h4>HOSPITALITY</h4>
            </div>
            <div class="col-md-2 col-sm-6">
              <h4>excellent</h4>
            </div>
            <div class="col-md-2 col-sm-6">
              <h4>spirit</h4>
            </div>
            <div class="col-md-2 col-sm-6">
              <h4>trust</h4>
            </div>
            <div class="col-md-2 col-sm-6">
              <h4>innovative</h4>
            </div>
            <div class="col-md-2 col-sm-6">
              <h4>attitude</h4>
            </div>
          </div>
        </div>
			</div>
		</div>

		<div class="section section__block content-properties">
			<div class="sectitle">
				<h2>Our Properties</h2>
			</div>
			<div class="row">
				<?php get_template_part('property'); ?>
			</div>
		</div>

		<div class="section section__block content-company">
			<div class="container">
				<div class="ico-company">
					<?php if( get_field('image') ): ?>
						<img src="<?php the_field('image'); ?>" />
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>

	<footer class="main-footer">
		<div class="footer-link text-center">

			<div class="footer-center">
				<div class="container">

					<?php

					if( have_rows('info-address') ):

						// loop through the rows of data
						while ( have_rows('info-address') ) : the_row(); ?>

							<div class="footer-address">
								<p><i class="icon-location"></i> <?php the_sub_field('address'); ?></p>
							</div>
							<div class="footer-phone">
								<p><i class="icon-phone"></i> <?php the_sub_field('phone'); ?></p>
							</div>
							<div class="footer-email">
								<p><i class="icon-envelope-open-o"></i> <a href="mailto: <?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></p>
							</div>

						<?php
						endwhile;

						else :

							// no rows found

						endif;

					?>

				</div>

			</div>
			<div class="social-footer text-center">

				<?php

				if( have_rows('social_media') ):

					// loop through the rows of data
					while ( have_rows('social_media') ) : the_row(); ?>

				<ul class="text-center">
					<li><a href="<?php the_sub_field('facebook'); ?>" class="icn-facebook" target="_blank"><i class="icon-facebook"></i></a></li>
					<li><a href="<?php the_sub_field('twitter'); ?>" class="icn-twitter" target="_blank"><i class="icon-twitter"></i></a></li>
					<li><a href="<?php the_sub_field('instagram'); ?>" class="icn-instagram" target="_blank"><i class="icon-instagram"></i></a></li>
					<li><a href="<?php the_sub_field('gplus'); ?>" class="icn-gplus" target="_blank"><i class="icon-gplus"></i></a></li>
				</ul>

				<?php
				endwhile;

				else :

					// no rows found

				endif;

			?>
			</div>
		</div>

		<div class="footer-info">
			<div class="container">
				<p class="verticent-inner">&copy; 2018 Powered by PT. Indohotels Booking Systems.</p>
			</div>
		</div>
		<!-- end .footer-info -->
	</footer>

	<script type="text/javascript" charset="utf-8">
	  $(window).load(function() {
	    $('.flexslider').flexslider({
				controlNav: false
			});
	  });
	</script>


</body>

</html>
